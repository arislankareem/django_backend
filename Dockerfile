FROM python:3
ENV PYTHONUNBUFFERED=1
WORKDIR /django_backend/ocean/
COPY requirements.txt  /django_backend/ocean/
RUN pip install --upgrade pip
RUN pip install gunicorn
RUN pip install -r requirements.txt
COPY . /django_backend/ocean/

EXPOSE 5000

cmd ["gunicorn", "--chdir", "ocean_dev", "--bind", ":5000", "ocean_dev.wsgi:application"]


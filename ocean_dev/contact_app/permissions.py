from rest_framework.permissions import BasePermission


class IsAdminOrCreateOnly(BasePermission):
    """
    Class for setting permission for giving Anonymous User permission only to create new data and giving permission for
    Admin User to read data
    """

    SAFE_METHODS = ['POST']

    def has_permission(self, request, view):
        if request.method in self.SAFE_METHODS:
            return True
        else:
            return request.user.is_superuser

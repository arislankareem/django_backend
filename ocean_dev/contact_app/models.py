from django.db import models
from django.db.models.signals import post_save
from django.conf import settings
from utils.utility import lead_send_email, contact_send_email


class LeadsModel(models.Model):
    """
    Model class for Leads Table(smv/supplier contact info)
    """

    first_name = models.CharField(max_length=100, blank=True)
    last_name = models.CharField(max_length=100, blank=True)
    company_name = models.CharField(max_length=200, blank=True)
    company_email = models.EmailField(max_length=200, blank=False)
    company_website = models.URLField(max_length=200, blank=True)
    phone_number = models.CharField(max_length=50, blank=True)
    company_registered_in = models.CharField(max_length=100, blank=True)
    annual_revenue = models.CharField(max_length=100, blank=True, default=None)
    description = models.TextField(blank=True)
    role = models.PositiveSmallIntegerField(choices=settings.ROLE_CHOICES, blank=True, null=True, default=1)

    def __str__(self):
        return self.company_email


class ContactModel(models.Model):
    """
    Model class for Contact Table (Contact form)
    """
    name = models.CharField(max_length=100, blank=True)
    mobile = models.CharField(max_length=50, blank=True)
    email_address = models.EmailField(max_length=200, blank=False)
    message = models.TextField(blank=True)

    def __str__(self):
        return self.email_address


def leads_send_admin_email(instance, **kwargs):
    """
    Initiating sending mails when LeadsModel is created
    """
    lead_send_email(subject="Leads Data", model_instance=instance, recipient_email=settings.ADMIN_EMAIL)


def contact_send_admin_email(instance, **kwargs):
    """
    Initiating sending mails when ContactModel is created
    """
    contact_send_email(subject="Contact Info Data", model_instance=instance, recipient_email=settings.ADMIN_EMAIL)


post_save.connect(leads_send_admin_email, sender=LeadsModel)

post_save.connect(contact_send_admin_email, sender=ContactModel)


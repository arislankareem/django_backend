from django.urls import path, include
from rest_framework.routers import DefaultRouter
from . import views


router = DefaultRouter()
router.register(r'lead', views.LeadsViewSet)
router.register(r'contact', views.ContactAddViewSet)

urlpatterns = [
    path('', include(router.urls))
]

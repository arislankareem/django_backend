from rest_framework import serializers
from .models import LeadsModel, ContactModel


class LeadsModelSerializers(serializers.ModelSerializer):
    """
    Serializer class for LeadsModel model
    """
    class Meta:
        model = LeadsModel
        fields = '__all__'


class ContactModelSerializers(serializers.ModelSerializer):
    """
    Serializer for ContactModel model
    """

    class Meta:
        model = ContactModel
        fields = '__all__'

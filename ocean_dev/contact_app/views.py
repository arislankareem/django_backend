from rest_framework.viewsets import ModelViewSet
from .permissions import IsAdminOrCreateOnly
from .models import LeadsModel, ContactModel
from .serializers import LeadsModelSerializers, ContactModelSerializers


class LeadsViewSet(ModelViewSet):
    """
    Class for CRUD operations in LeadsModel
    """
    permission_classes = [IsAdminOrCreateOnly]
    queryset = LeadsModel.objects.all()
    serializer_class = LeadsModelSerializers


class ContactAddViewSet(ModelViewSet):
    """
    Class for CRUD operations in the ContactModel
    """
    permission_classes = [IsAdminOrCreateOnly]
    queryset = ContactModel.objects.all()
    serializer_class = ContactModelSerializers



import os
import sys
from django.core.mail import send_mail
import logging
from django.conf import settings
from django.template.loader import render_to_string


def lead_send_email(subject, model_instance, recipient_email):
    """
    Function for sending email to the admin email(on adding a new data in LeadsModel)

    :param subject: subject of the email
    :param model_instance: model instance
    :param recipient_email: email id of recipient
    :return:
    """
    try:
        message = render_to_string('contacts_app/leads_info.html', {
            'leads_data': model_instance
        })
        send_mail(subject=subject, message=message, recipient_list=[recipient_email], fail_silently=False,
                  from_email=settings.EMAIL_HOST_USER, html_message=message)
    except Exception as error:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        f_name = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(f"Utility lead_send_emails error - {exc_type}, {f_name}, {exc_tb.tb_lineno} {str(error)}")


def contact_send_email(subject, model_instance, recipient_email):
    """
    Function for sending email to the admin email(on adding a new data in ContactModel)

    :param subject: subject of the email
    :param model_instance: model instance
    :param recipient_email: email id of recipient
    :return:
    """
    try:
        message = render_to_string('contacts_app/contact_info.html', {
            'contact_data': model_instance
        })
        send_mail(subject=subject, message=message, recipient_list=[recipient_email], fail_silently=False,
                  from_email=settings.EMAIL_HOST_USER, html_message=message)
    except Exception as error:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        f_name = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        logging.error(f"Utility contact_send_emails error - {exc_type}, {f_name}, {exc_tb.tb_lineno} {str(error)}")
